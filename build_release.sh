#!/bin/bash
build_directory=build-dir
build_type=release

mkdir ${build_directory} 2> /dev/null

cd ${build_directory}
rm -r ${build_type}  2> /dev/null
mkdir ${build_type}

cd ${build_type}
cmake -DCMAKE_BUILD_TYPE=Release  ../../Garrison
make
