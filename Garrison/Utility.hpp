#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <string.h>

const auto port = 8080;

// Client can send a max of 1 KB
const auto clientMaxMessageSize = 1024;

// Size of digest for sha1 specified in gcrpyt manual section 7.1
// https://www.gnupg.org/documentation/manuals/gcrypt.pdf
const auto sha1DigestSize = 20;
const auto serverMessageSize = sha1DigestSize;

void PrintSocketError(const std::string& errorMsg, int errorNum)
{
	std::cerr
	<< "Error: "
	<< strerror(errorNum)
	<< " | "
	<< errorMsg
	<< std::endl;
}

auto IsSocketError(ssize_t result)
{
	return result < 0;
}

auto SetupSocket()
{
	const auto socketFD = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketError(socketFD))
	{
		PrintSocketError("creating socket" , errno);
		return -1;
	}

	const auto opt = 1;

	auto result =
		setsockopt(
			socketFD,
			SOL_SOCKET,
			SO_REUSEADDR | SO_REUSEPORT,
			&opt,
			sizeof(opt));

	if (IsSocketError(socketFD))
	{
		PrintSocketError("setting socket options", errno);
		return -1;
	}

	return socketFD;
}

auto SocketHasClosed(ssize_t result)
{
	return result == 0;
}

#endif // UTILITY_HPP
