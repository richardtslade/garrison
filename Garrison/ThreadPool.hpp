#ifndef THREADPOOL_HPP
#define THREADPOOL_HPP

#include <queue>
#include <functional>
#include <mutex>
#include <atomic>
#include <thread>
#include <condition_variable>

class ThreadPool
{
private:
	using ThreadJob =
		std::function<void()>;
public:
	ThreadPool(uint32_t numThreads)
	{
		Start(numThreads);
	}

	~ThreadPool()
	{
		m_shutdown = true;

		m_signal.notify_all();

		for(auto& t : m_threadPool)
		{
			t.join();
		}
	}

	void AddWork(const ThreadJob& fn)
	{
		std::lock_guard<std::mutex> l(m_mutex);
		m_jobs.push(fn);

		m_signal.notify_all();
	}

private:
	void Start(uint32_t numThreads)
	{
		m_threadPool.reserve(numThreads);

		for(auto i = 0u; i < numThreads; ++i)
		{
			m_threadPool.push_back(std::thread(std::bind(&ThreadPool::PollForJobs, this)));
		}
	}

	void Stop()
	{
		m_shutdown = true;
	}

	void PollForJobs()
	{
		while(!m_shutdown)
		{
			ThreadJob job = nullptr;

			{
				std::unique_lock<std::mutex> l(m_mutex);

				auto predicate =
					[this]()
				{
					return !m_jobs.empty() || m_shutdown;
				};

				m_signal.wait(l, predicate);

				if(!m_jobs.empty())
				{
					job = m_jobs.front();
					m_jobs.pop();
				}
			}

			if(job)
			{
				job();
			}

			m_signal.notify_all();
		}
	}

	std::queue<ThreadJob> m_jobs;
	std::condition_variable m_signal;
	std::mutex m_mutex;
	std::vector<std::thread> m_threadPool;
	std::atomic_bool m_shutdown{false};
};

#endif // THREADPOOL_HPP
