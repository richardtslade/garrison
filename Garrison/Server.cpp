#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>

#include <iostream>
#include <string>
#include <vector>
#include <cassert>

#include <gcrypt.h>

#include "ThreadPool.hpp"
#include "Utility.hpp"

void PrintGCryptoError(
	const std::string& errorMsg,
	gcry_error_t error)
{
	std::cerr
	<< "Error: "
	<< gcry_err_code(error)
	<< " from source: "
	<< gcry_err_source(error)
	<< " | "
	<< errorMsg
	<< std::endl;
}

void HandleClientMessages(int clientSocketFD)
{
	std::cout
	<< "Client connected socket id: "
	<< clientSocketFD
	<< std::endl;

	while(true)
	{
		unsigned char readBuffer[clientMaxMessageSize]{0};

		ssize_t result = read(clientSocketFD, readBuffer, clientMaxMessageSize);

		if(IsSocketError(result))
		{
			PrintSocketError("reading from socket", errno);
			break;
		}
		else if(SocketHasClosed(result))
		{
			break;
		}

		using Buffer =
			std::vector<unsigned char>;

		Buffer buffer;

		auto index = 0u;
		unsigned char currentChar = readBuffer[index++];

		while(currentChar != '\0'
			&& index < clientMaxMessageSize)
		{
			buffer.emplace_back(currentChar);
			currentChar = readBuffer[index++];
		}

		const static auto hashingAlgo = GCRY_MD_SHA1;

		gcry_md_hd_t handle;
		auto error = gcry_md_open(&handle, hashingAlgo, 0);

		if(error != GPG_ERR_NO_ERROR)
		{
			PrintGCryptoError("opening hashing handle", error);
			break;
		}

		const static auto numberOfHashes = 10000u;

		for(auto i = 0u; i < numberOfHashes; ++i)
		{
			for(const auto c : buffer)
			{
				gcry_md_putc(handle, c);
			}

			buffer.clear();

			auto digest = gcry_md_read(handle, hashingAlgo);

			buffer = Buffer(digest, digest + sha1DigestSize);

			gcry_md_reset(handle);
		}

		gcry_md_close(handle);

		assert(buffer.size() == serverMessageSize);

		result =
			send(clientSocketFD , buffer.data(), serverMessageSize, MSG_NOSIGNAL);

		if(IsSocketError(result))
		{
			PrintSocketError("send to socket", errno);
			break;
		}
	}

	std::cout
	<< "Client disconnected socket id: "
	<< clientSocketFD
	<< std::endl;

	close(clientSocketFD);
}

int main()
{
	const int socketFD = SetupSocket();

	if(IsSocketError(socketFD))
	{
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);

	const int addrlen = sizeof(address);

	// Bind file descripter to socket
	auto result =
		bind(
			socketFD,
			(struct sockaddr *)&address,
			sizeof(address));

	if (IsSocketError(result))
	{
		PrintSocketError("binding socket", errno);
		exit(EXIT_FAILURE);
	}

	const static int numberOfThreads = 20;

	// Listen on socket
	result = listen(socketFD, numberOfThreads);

	if (IsSocketError(result))
	{
		PrintSocketError("starting listen on socket", errno);
		exit(EXIT_FAILURE);
	}

	std::cout
	<< "Server is listening for connections on port: "
	<< port
	<< std::endl
	<< std::endl;

	// Init GCrypt
	static const auto minimumGCryptVersion{"1.8.1"};

	if(!gcry_check_version(minimumGCryptVersion))
	{
		std::cerr
		<< "Libcrypt version: "
		<< gcry_check_version(nullptr)
		<< " is too old."
		<< std::endl
		<< " Need minimum version: "
		<< minimumGCryptVersion
		<< std::endl;

		exit(EXIT_FAILURE);
	}

	auto error = gcry_control(GCRYCTL_DISABLE_SECMEM, 0);

	if (error != GPG_ERR_NO_ERROR)
	{
		PrintGCryptoError("disabling secure memory", error);
		exit(EXIT_FAILURE);
	}

	error = gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);

	if (error != GPG_ERR_NO_ERROR)
	{
		PrintGCryptoError("finishing initialisation", error);
		exit(EXIT_FAILURE);
	}

	ThreadPool threadPool(numberOfThreads);

	// Start accepting connection requests
	int newClientSocket =
		accept(
			socketFD,
			(struct sockaddr *)&address,
			(socklen_t*)&addrlen);

	while(!IsSocketError(newClientSocket))
	{
		threadPool.AddWork(
			std::bind(
				HandleClientMessages,
				newClientSocket));

		newClientSocket =
			accept(
				socketFD,
				(struct sockaddr *)&address,
				(socklen_t*)&addrlen);
	}

	std::cout
	<< std::endl
	<< "Server closing"
	<< std::endl;

	close(socketFD);

	return 0;
}
