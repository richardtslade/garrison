#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>

#include <iostream>
#include <sstream>
#include <iomanip>

#include "Utility.hpp"

namespace
{
std::string MessageToHex(unsigned char* msg, uint8_t lenght)
{
	std::stringstream ss;
	ss << std::hex;

	for(auto i = 0u; i < lenght; ++i)
	{
		ss
		<< std::setw(2)
		<< std::setfill('0')
		<< static_cast<int>(msg[i])
		<< " ";
	}

	return ss.str();
}
} // namespace anon

int main(int /*argc*/, char const */*argv*/[])
{
	std::cout
	<< "Welcome:"
	<< std::endl
	<< "- Type Exit/exit to close client"
	<< std::endl
	<< "- Or enter string to be hashed:"
	<< std::endl
	<< std::endl;

	const auto socketFD = SetupSocket();

	if(IsSocketError(socketFD))
	{
		exit(EXIT_FAILURE);
	}

	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);

	// Convert IPv4 and IPv6 addresses from text to binary form
	auto result = inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);

	if(IsSocketError(result))
	{
		PrintSocketError("Invalid address or address not supported", errno);
		close(socketFD);

		exit(EXIT_FAILURE);
	}

	result =
		connect(
			socketFD,
			(struct sockaddr *)&serv_addr,
			sizeof(serv_addr));

	if (IsSocketError(result))
	{
		PrintSocketError("connecting to server", errno);
		close(socketFD);

		exit(EXIT_FAILURE);
	}

	while(true)
	{
		std::string userInput;

		std::getline(std::cin, userInput);

		if(userInput.empty())
		{
			continue;
		}
		else if(userInput.size() > clientMaxMessageSize)
		{
			std::cerr
			<< "Input too large, ignoring"
			<< std::endl;

			continue;
		}
		else if(userInput == "Exit"
			|| userInput == "exit")
		{
			break;
		}

		auto result =
			send(
				socketFD ,
				userInput.data(),
				userInput.size(),
				MSG_NOSIGNAL);

		if(IsSocketError(result))
		{
			PrintSocketError("sending to socket", errno);
			break;
		}

		std::cout
		<< std::endl
		<< "Sent message: "
		<< std::endl
		<< userInput
		<< std::endl
		<< "------------------"
		<< std::endl;

		unsigned char buffer[serverMessageSize]{0};

		result = read(socketFD , buffer, serverMessageSize);

		if(IsSocketError(result))
		{
			PrintSocketError("reading from socket", errno);
			break;
		}
		else if(SocketHasClosed(result))
		{
			std::cout
			<< "Socket: "
			<< socketFD
			<< " has closed"
			<< std::endl;

			break;
		}

		std::cout
		<< "Received message: "
		<< std::endl
		<< MessageToHex(buffer, serverMessageSize)
		<< std::endl
		<< std::endl;
	}

	close(socketFD);

	return 0;
}
